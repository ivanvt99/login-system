<?php

//Grab the data


if (isset($_POST["login"]))
{
    $username =  $_POST["username"];
    $password =  $_POST["password"];
}

//make an Instance of the Controller
include "classes/LoginDatabase.php";
include "classes/Login.php";
include "classes/LoginController.php";

$login = new LoginController($password, $username);

//Run the error handlers
$login->loginUser();
// Go to the front page
header("location: ../index.php?error=none");
