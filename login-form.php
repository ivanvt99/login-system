<?php if ($loggedIn) : ?>
<div class="d-flex justify-content-center title-container">
    <h1 id="title" class="title text-center">Welcome back, <?php echo $username; ?>!</h1>
</div>

<div class="logout d-flex justify-content-center">
    <a href="logout.php">LOGOUT</a>
</div>
<?php else : ?>
    <div class="d-flex justify-content-center">
        <h1 id="title" class="title text-center">Log in</h1>
    </div>

    <div class="login">
        <div class="form-container">
            <form action="" method="POST"
                  class="form-control d-flex justify-content-between align-items-center login-form">

                <div class="form-wrapper h-100  w-100 ">
                    <div class="form-input">
                        <label for="username">Username</label>
                        <input id="username" type="email" placeholder="johndoe@example.com">
                        <span id="username-error" class="error d-none"> test</span>
                    </div>

                    <div class="form-input">
                        <label for="password">Password</label>
                        <input id="password" type="password"  placeholder="enter your password">
                        <span id="password-error" class="error d-none"> test</span>
                        <div class="show-pass">
                            <i id="show" class="fas fa-eye" onclick="showPassword()"></i>

                        </div>
                    </div>

                    <div class="form-input">
                        <a id="login"  class="btn login-button"  onclick="submit()"> Log in</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
<?php endif; ?>