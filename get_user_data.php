<?php
session_start();

if (isset($_SESSION["userId"]) && isset($_SESSION["username"])) {
    $userData = [
        "userId" => $_SESSION["userId"],
        "username" => $_SESSION["username"]
    ];

    header('Content-Type: application/json');
    echo json_encode($userData);
} else {
    echo json_encode(["error" => "Session data not found."]);
}
?>
