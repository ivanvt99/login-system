<?php

class Login extends LoginDatabase
{
    protected function getUser($username, $password)
    {
        /** testing the other method*/
        $statement = $this->connect()->prepare('SELECT password FROM users WHERE username = ?;');

        if (!$statement) {

            $additionalInfo = "Statement failed.";

            header("HTTP/1.1 400 Bad Request");
            header("Content-Type: application/json");

            echo json_encode(["error" => $additionalInfo]);
            exit();
        }

        if (!$statement->execute([$username])) {

            $statement = null;

            $additionalInfo = "Statement failed.";

            header("HTTP/1.1 400 Bad Request");
            header("Content-Type: application/json");

            echo json_encode(["error" => $additionalInfo]);
            exit();
        }

// Successful login


        if ($statement->rowCount() == 0) {
            $statement = null;

            $additionalInfo = "The request is invalid. User not found.";

            header("HTTP/1.1 400 Bad Request");
            header("Content-Type: application/json");

            echo json_encode(["error" => $additionalInfo]);
            exit();

        }

        $passwordHashed = $statement->fetchAll(PDO::FETCH_ASSOC);

        //for testing purposes...
        $testPassDB = $passwordHashed[0]["password"];

        //returns false
        $checkPassword = password_verify($password, $passwordHashed[0]["password"]);

        //checking with unhashed password
        if ($password !== $testPassDB) {
            $statement = null;
            $additionalInfo = "Incorrect password.";

            header("HTTP/1.1 400 Bad Request");
            header("Content-Type: application/json");

            echo json_encode(["error" => $additionalInfo]);
            exit();
        } elseif ($password === $testPassDB) {
            $statement = $this->connect()->prepare('SELECT * FROM users WHERE username = ? AND password = ?;');

            if (!$statement->execute(array($username, $password))) {
                $statement = null;

                header("location: ../index.php?error=statementFailed");
                exit();
            }
            // login the user...

            if ($statement->rowCount() > 0) {
                $user = $statement->fetchAll(PDO::FETCH_ASSOC);
                session_start();
                $_SESSION["userId"] = $user[0]["id"];
                $_SESSION["username"] = $user[0]["username"];
            }
        }

    }
}