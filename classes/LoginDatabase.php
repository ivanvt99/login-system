<?php

class LoginDatabase
{
    protected function connect()
    {
        try {
            $username = "root";
            $password = "";
            $loginDb = new PDO('mysql:host=localhost;dbname=logindb', $username, $password);
            return $loginDb;
        }
        catch (PDOException $e)
        {
          print "Connection Error!".$e->getMessage(). "<br>";
          die();
        }
    }
}