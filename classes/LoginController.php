<?php

class LoginController extends Login
{
//    private $uid;
    private $username;
    private $password;

    public function __construct( $password, $username)
    {
        $this->username = $username;
        $this->password = $password;
    }


    public function loginUser(){
        if($this->emptyInput() == false)
        {
           header("location: ../index.php?error=emptyInput");
           exit();
        }

        if($this->invalidUsername() == false)
        {
            $additionalInfo = "Invalid username.";

            header("HTTP/1.1 400 Bad Request");
            header("Content-Type: application/json");

            echo json_encode(["error" => $additionalInfo]);
            exit();
        }

        $this->getUser($this->username, $this->password);
    }


    private function emptyInput()
    {
        $result = null;

        if (empty( $this->username || $this->password )) {
            $result = false;
        } else {
            $result = true;
        }
        return $result;
    }

    private function invalidUsername() {
        $result = null;
        if (!filter_var($this->username, FILTER_VALIDATE_EMAIL))
        {
            $result = false;
        }
        else
        {
            $result =  true;
        }

        return $result;
    }


}