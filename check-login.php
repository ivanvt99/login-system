<?php
session_start(); // Start the session

// Login check
if (isset($_SESSION["userId"])) {
    $loggedIn = true;
    $userId = $_SESSION["userId"];
    $username = $_SESSION["username"];
} else {
    $loggedIn = false;
}
?>
