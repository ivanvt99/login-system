function showPassword() {

    if ('password' == $('#password').attr('type')) {
        $('#password').prop('type', 'text');
        $('#show').removeClass('fa-eye');
        $('#show').addClass('fa-eye-slash');
    } else {
        $('#password').prop('type', 'password');
        $('#show').removeClass('fa-eye-slash');
        $('#show').addClass('fa-eye');
    }
}

function submit() {
    let username = $('#username').val();
    let password = $('#password').val();

    if ($.trim(username).length > 0 && $.trim(password).length > 0) {
        $.ajax({
            url: 'login.php',
            method: 'POST',
            data: {
                login: 1,
                username: username,
                password: password,
            },
            success: function () {
              loginSuccess();
            },

            //TODO get the error messages and check php files !!!
            // error: function(xhr) {
            //     console.log(xhr.responseJSON.error);
            // },
            dataType: 'text'
        });

        function loginSuccess() {
            $('.login').addClass('d-none');
            $.ajax({
                url: 'get_user_data.php',
                method: 'GET',
                dataType: 'json',
                success: function(data) {
                    if (data.error) {

                    } else {

                        console.log("User ID:", data.userId);
                        console.log("Username:", data.username);

                        $("#title").text("Welcome back,"+ " " + data.username + "!").addClass('text-center');

                        $(".container").append("<div class=\"logout d-flex justify-content-center title-container\">\n" +
                            "    <a href=\"logout.php\">LOGOUT</a>\n" +
                            "</div>");
                    }
                },
                error: function(xhr, status, error) {

                }
            });
        }
    }
    else
    {
        if ($.trim(username).length === 0)
        {
            $("#username-error").removeClass('d-none').text("Username is required");
        }
        if ($.trim(password).length === 0)
        {
            $("#password-error").removeClass('d-none').text("Password is required");
            $('.show-pass').css("bottom", "45px");
        }
    }
}
