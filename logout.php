<?php

session_start();
session_unset();
session_destroy();

//Go to the login page...
header("location: index.php?error=none");